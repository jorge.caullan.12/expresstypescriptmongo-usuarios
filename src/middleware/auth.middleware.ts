import { NextFunction, Response } from 'express';
import * as jwt from 'jsonwebtoken';
// import AuthenticationTokenMissingException from '../exceptions/AuthenticationTokenMissingException';
// import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException';
import DataStoredInToken from '../interfaces/dataStoredInToken';
import RequestWithUser from '../interfaces/requestWithUser.interface';
import userModel from '../users/user.model';
 
async function authMiddleware(request: RequestWithUser, response: Response, next: NextFunction) {
  const cookies = request.cookies;
  if (cookies && cookies.Authorization) {
    const secret = process.env.JWT_SECRET;
    try {
      const verificationResponse = jwt.verify(cookies.Authorization, secret) as DataStoredInToken;
      const id = verificationResponse._id;
      const user = await userModel.findById(id);
      if (user) {
        request.user = user;
        // response.locals.user = user;
        // console.log(user);
        next();
      } else {
        response.status(400).send({error: 'Wrong authentication token'});
        // next(new WrongAuthenticationTokenException());
      }
    } catch (error) {
      response.status(400).send({error: 'Wrong authentication token'});
      // next(new WrongAuthenticationTokenException());
    }
  } else {
    response.status(400).send({error: 'Authentication token missing (no cookies found)'});
    // next(new AuthenticationTokenMissingException());
  }
}
 
export default authMiddleware;