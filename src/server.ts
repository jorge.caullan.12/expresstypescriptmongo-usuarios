// import * as express from 'express';
// import * as bodyParser from 'body-parser';
import App from './app';
import PostController from './posts/posts.controller';
import AuthenticationController from './authentication/authentication.controller';
import validateEnv from './utils/validateEnv';

validateEnv();

// function loggerMiddleware(request: express.Request, response: express.Response, next) {
//   console.log(`${request.method} ${request.path}`);
//   next();
// }

// const app = express();
// const router = express.Router();

const app = new App(
  [
    new PostController(),
    new AuthenticationController(),
  ],
);

// app.use(loggerMiddleware);
// app.use(bodyParser.json());

// router.get('/', (request, response) => {
//   response.send({
//     hostname: request.hostname,
//     path: request.path,
//     method: request.method,
//   });
// });

// app.use('/api', router);

// app.post('/', (request, response) => {
//  response.send(request.body);
// })

app.listen();