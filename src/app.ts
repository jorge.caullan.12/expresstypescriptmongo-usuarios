import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';  
import * as express from 'express';
import * as mongoose from 'mongoose';
import Controller from './interfaces/controller.interface';
import errorMiddleware from './middleware/error.middleware';
import 'dotenv/config';

class App {
  public app: express.Application;
 
  constructor(controllers: Controller[]) {
    this.app = express();
 
    this.connectToTheDatabase();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }
 
  public listen() {
    this.app.listen(process.env.PORT, () => {
      console.log(`App listening on the port ${process.env.PORT}`);
    });
  }
 
  private initializeMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use(cookieParser());
  }
 
  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }
 
  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.router);
    });
  }

  private connectToTheDatabase() {
    const {
      MONGO_USER,
      MONGO_PASSWORD,
      MONGO_PATH,
    } = process.env;
    // mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_PATH}`, {auth:{authdb:"admin"}});
    mongoose.connect(`mongodb://${MONGO_PATH}/?authSource=test&w=1`, {
      user:`${MONGO_USER}`,
      pass:`${MONGO_PASSWORD}`,
    });
    // mongoose.connect(`mongodb://${MONGO_PATH}`);
    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
  }
}

export default App;