import * as bcrypt from 'bcrypt';
import * as express from 'express';
import * as jwt from 'jsonwebtoken';
// import UserWithThatEmailAlreadyExistsException from '../exceptions/UserWithThatEmailAlreadyExistsException';
// import WrongCredentialsException from '../exceptions/WrongCredentialsException';
import Controller from '../interfaces/controller.interface';
import validationMiddleware from '../middleware/validation.middleware';
import authMiddleware from '../middleware/auth.middleware';
import CreateUserDto from '../users/user.dto';
import userModel from './../users/user.model';
import LogInDto from './logIn.dto';
import DataStoredInToken from '../interfaces/dataStoredInToken';
import TokenData from '../interfaces/tokenData';
import User from '../users/user.interface';
import 'dotenv/config';
 
class AuthenticationController implements Controller {
  public path = '/auth';
  public router = express.Router();
  private user = userModel;
 
  constructor() {
    this.initializeRoutes();
  }
 
  private initializeRoutes() {
    this.router.post(`${this.path}/register`, validationMiddleware(CreateUserDto), this.registration);
    this.router.post(`${this.path}/login`, validationMiddleware(LogInDto), this.loggingIn);
    this.router.post(`${this.path}/logout`, this.loggingOut);
    this.router.get(`/currentUser`, authMiddleware, this.currentUser)
    this.router.patch(`/updateMe`, authMiddleware, this.updateMe)
  }

  private createToken(user: User): TokenData {
    const expiresIn = 60 * 60; // an hour
    const secret = process.env.JWT_SECRET;
    const dataStoredInToken: DataStoredInToken = {
      _id: user._id,
    };
    return {
      expiresIn,
      token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
    };
  }
  
  private createCookie(tokenData: TokenData) {
    return `Authorization=${tokenData.token}; HttpOnly; Max-Age=${tokenData.expiresIn}`;
  }
 
  private registration = async (request: express.Request, response: express.Response) => {
    const userData: CreateUserDto = request.body;
    if (
      await this.user.findOne({ email: userData.email })
    ) {
      // next(new UserWithThatEmailAlreadyExistsException(userData.email));
      response.status(400).send({error: 'User email already exists'});
    } else {
      const hashedPassword = await bcrypt.hash(userData.password, 10);
      const user = await this.user.create({
        ...userData,
        password: hashedPassword,
      });
      user.password = undefined;
      const tokenData = this.createToken(user);
      response.setHeader('Set-Cookie', [this.createCookie(tokenData)]);
      response.send(user);
    }
  }

  private currentUser = async (request: express.Request, response: express.Response) => {
    const user = request['user'];
    user.password = undefined;
    response.send(user);
  }
 
  private updateMe = async (request: express.Request, response: express.Response) => {
    const id = request['user']['_id'];
    const userData: User = request.body;
    let userHashedData = userData

    if (userData.password){
      const hashedPassword = await bcrypt.hash(userData.password, 10);
      userHashedData = {
        ...userData,
        password: hashedPassword,
      };
    }
    this.user.findByIdAndUpdate(id, userHashedData, { new: true })
      .then((user) => {
        user.password = undefined;
        response.send(user);
      })
      .catch((e) => {
        response.status(400).send({error: "New credentials missings"})
      });
  }
 
  private loggingIn = async (request: express.Request, response: express.Response) => {
    const logInData: LogInDto = request.body;
    const user = await this.user.findOne({ email: logInData.email });
    if (user) {
      const isPasswordMatching = await bcrypt.compare(logInData.password, user.password);
      if (isPasswordMatching) {
        user.password = undefined;
        const tokenData = this.createToken(user);
        response.setHeader('Set-Cookie', [this.createCookie(tokenData)]);
        response.send({
          ...user._doc,
          token: tokenData
        });
      } else {
        response.status(400).send({error: 'Wrong credentials'});
      }
    } else {
      response.status(400).send({error: 'Wrong credentials'});
    }
  }

  private loggingOut = (request: express.Request, response: express.Response) => {
    response.setHeader('Set-Cookie', ['Authorization=;Max-age=0']);
    response.send(200);
  }
}
 
export default AuthenticationController;